package com.example.androidretrofitrx2;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseModel {
    private String name;
    private String region;
    private String capital;
    private String area;

    public String getRegion() {
        return region;
    }

    public String getCapital() {
        return capital;
    }

    public String getArea() {
        return area;
    }

    public ResponseModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
