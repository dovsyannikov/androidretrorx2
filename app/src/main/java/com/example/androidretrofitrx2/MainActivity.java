package com.example.androidretrofitrx2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.txtCountryCode)
    EditText txtCountryCode;
    @BindView(R.id.smokeInformation)
    TextView smokeInformation;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Info by country code");
        ButterKnife.bind(this);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disposable = ApiService.getCountryByName(txtCountryCode.getText().toString())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<List<ResponseModel>>() {
                            @Override
                            public void accept(List<ResponseModel> responseModels) throws Exception {
//                                StringBuilder stringBuilder = new StringBuilder();
//                                stringBuilder.append("Country name: " + responseModels.get(0).getName());
//                                stringBuilder.append("\n");
//                                stringBuilder.append("Region: " + responseModels.get(0).getRegion());
//                                stringBuilder.append("\n");
//                                stringBuilder.append("Capital: " + responseModels.get(0).getCapital());
//                                stringBuilder.append("\n");
//                                stringBuilder.append("Area: " + responseModels.get(0).getArea());
                                smokeInformation.setText(responseModels.get(0).toString());

                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Toast.makeText(MainActivity.this, "Smth went wrong", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }
}
